export const environment = {
  production: true,
  api: 'https://op9ceehfa9.execute-api.eu-central-1.amazonaws.com/final/funktion',
  threshold: 0.90,
};
