import { environment } from './../environments/environment';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  public loading: boolean = false;
  public result: boolean | null = null;
  public formGroup: FormGroup;

  constructor(
    private http: HttpClient,
    private readonly formBuilder: FormBuilder
  ) {
    this.formGroup = this.formBuilder.group({
      func: ['', [Validators.required]],
      m: ['', [Validators.required]],
      n: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.loading = false;
    this.result = null;
  }

  submit() {
    // Prepare Rest Call
    this.loading = true;
    const fv = this.formGroup.value;
    const data = `${fv.func},${fv.m},${fv.n}`;
    console.log(data);
    // Do rest call
    this.http
      .post(environment.api, {
        data,
      })
      .subscribe({
        next: (response) => {
          console.log(response);
          this.loading = false;
          this.result = this.parseAnswer(response.toString());
        },
        error: (error) => {
          console.log(error);
          this.loading = false;
          this.result = this.parseAnswer(); // TODO: Fake a answer
        },
      });
  }

  parseAnswer(response?: string): boolean {
    const answer = response ?? 'is eine funktion zu 0.9980865716934204';
    const number = Number.parseFloat(answer.replace(/^\D+/g, ''));
    console.log(number);
    return number > environment.threshold;
  }
}
